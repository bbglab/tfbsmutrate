#!/usr/bin/perl 

# script to collect XRseq map result of DHS centered analysis
# written by Sabarinathan Radhakrishnan

#use warnings 'all';
use strict;
use FindBin;

# read config file to get the main path 
open(IN,"$FindBin::Bin/config.file") or die ("error in opening the config file");
my %VAR; # get variables from config file
while(<IN>){chomp;$_=~s/\s+//g;if($_=~/=/){my @s=split(/\=/, $_);$VAR{$s[0]}=$s[1];}}
close IN;
die "error in retreiving the main path $VAR{path}", if(! -d $VAR{path});

use Getopt::Std;
sub usage(){print "$0 -c <cancerType> -a <analysisType> -o <outputDir>\n";exit;}

my %opts;
getopt('sctaoe',\%opts);

&usage, if(scalar(keys(%opts))>3 || scalar(keys(%opts))<3);

my $ctype=$opts{c}; # cancer type
my $atype=$opts{a}; # DHS or DHS subset
my $moutput=$opts{o}; # output metafile directory
my $target="dhsCentered";

my @cellTypes=qw(CPD1h CSB64 CSBCPD PP641h);
my %names=qw(CPD1h NHF1_CPD CSBCPD CSB_CPD CSB64 CSB_PP64 PP641h NHF1_PP64);
my @strands=qw(PLUS MINUS);

# assign full name for each analysis type
my %dhsNames=qw(all all-DHS noPromoter_noTFBS DHS-noPromoters-noTFBS Promoter_noTFBS DHS-Promoters-noTFBS Promoter_TFBS DHS-Promoters-TFBS Promoter_predTFBS DHS-Promoters-predTFBS noPromoter_predTFBS DHS-noPromoters-predTFBS noPromoter_TFBS DHS-noPromoters-TFBS noPromoter_predTFBSAll DHS-noPromoters-predTFBSAll Promoter_predTFBSAll DHS-Promoters-predTFBSAll);

# results directory
my $path="$VAR{path}/results/$target/$ctype/xr-seq/";

# output directory
my $opath="$VAR{path}/results/$moutput/xr-seq";
`mkdir $opath`, if(! -d $opath);

my $flank=$VAR{flank};
print $flank,"\n";

foreach my $cell (@cellTypes)
{ 
  my %hash=();
  my %tflist;
  print $cell,"\n";
  # read the average repair rate for each DHS subset  and two strands, and save it in a hash
  foreach my $tfFile (`ls $path/$cell/*.csv`)
  { 
    chomp $tfFile;
    my $tf;my $st;
    my @s=split(/\_/, (split(/\//, $tfFile))[-1]);
    $tf=$dhsNames{join("_", @s[2..($#s-2)])};
    $st=$s[$#s];
    $st=~s/\.bed\.csv//g;
    # initialize
    for(my $i=-$flank;$i<=$flank;$i++)
    { $hash{$i}->{$tf}->{$st}=0; }

    # per TF output
    open(IN, $tfFile ) or die "error in opening hte input file $tfFile";
    while(<IN>)
    { 
      next, if ($_=~/motif/);
      my @s=split(/\s+/, $_);
      $hash{$s[1]}->{$tf}->{$st}=$s[4];
    }
    close IN;
    $tflist{$tf}=1;
  }

  # print a metafile with repair rate for all DHS subsets
  foreach my $str (@strands)
  {
   open(OUT,">$opath/$atype\_$names{$cell}\_$str.csv");
   print OUT "position\t",join("\t",sort(keys(%tflist))),"\n";
   for(my $i=-$flank;$i<=$flank;$i++)
   { 
    my $val="$i";
    # per TF mapping
    foreach my $tf (sort(keys(%tflist)))
    { 
      $val.= "\t". $hash{$i}->{$tf}->{$str};
    }
    print OUT $val,"\n";
   }
   close OUT;
   `gzip -f $opath/$atype\_$names{$cell}\_$str.csv`;
  }
  
  # print a file for combined strand value 
  open(OUT,">$opath/$atype\_$names{$cell}_combined.csv");
  print OUT "position\t",join("\t",sort(keys(%tflist))),"\n";
  # combine two strands 
  for(my $i=-$flank;$i<=$flank;$i++)
  { 
    my $val="$i";
    # per TF mapping
    foreach my $tf (sort(keys(%tflist)))
    { 
      my $avg = $hash{$i}->{$tf}->{"PLUS"}+$hash{$i}->{$tf}->{"MINUS"};
      $val.="\t".($avg/2);
    }
    print OUT $val,"\n";
  }
  close OUT;

  `gzip -f $opath/$atype\_$names{$cell}_combined.csv`;
}
