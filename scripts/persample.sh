#!/bin/bash


# script to compute mutation rate at TFBS centered region per sample
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -c <cancertype> -t <targetAnalysis> -a <analysisType>
	-h  display this help and exit
	-c  cancer type (e.g., skcm)
	-t  target analysis (proximal)
	-a  TFBS overlap with DHS or not (DHS|noDHS)
Example: ${0##*/} -c skcm -t proximal -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "c:t:a:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
        a) atype=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

# make a tmp directory
if [ -d "/scratch/" ]; then 
   tmpdir=$(mktemp -d -p /scratch);
 else
   tmpdir=$(mktemp -d);
fi

# target matches
declare -A tmatch=(["proximal"]="tfbs-proximal" ["distal"]="tfbs-distal" ["bindStrength"]="tfbs-bs-seperated")
target=${tmatch[$target]} 

# main paths
perSamplePath="$path/results/$target/$ctype/perSample"

# mutation mapping
for id in $(zcat $mutpath/${ctype}.txt.gz | cut -f6 | sort -u);
do
echo $id;
zgrep -w $id $mutpath/${ctype}.txt.gz | $bedtools/intersectBed -a $perSamplePath/${atype}_mutated.bed.gz -b stdin -f 1.0 -c >$tmpdir/${id}_${atype}.bed

# mutation count
outfile="$tmpdir/${id}_${atype}.cnt"
perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[6];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$cnt{$d},"\t",$bp{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype $tmpdir/${id}_${atype}.bed >$outfile

gzip -9 $tmpdir/${id}_${atype}.bed
mv $tmpdir/*.gz $outfile $perSamplePath/.
done

echo "generating final output file..."
# compute the results in a csv file
$path/scripts/get_results_persample.pl $ctype $atype $path $perSamplePath

rm -rf $tmpdir
