#!/bin/bash

# script to compute the mutational probabilities for background computation

# read input parameters
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

# create a output directory to save intermediate files
echo $mutpath/forsignature
outPath="$mutpath/forsignature";
if [ ! -d $outPath ];then mkdir $outPath;fi

# go over each cancer type and compute the probabilities of each mutation type with its tri-nucleotide context
for ctype in skcm brca luad lusc crc; # specifiy which cancer types to compute, for tcga505 
do
  # get DHS file for the given ctype
  dnaseFile=`grep -w $ctype $dnasePath/DHS_list.txt | cut -f1,3`;

  arr=($dnaseFile); # cancer type name[0] and its matching dnase id on [1]
  echo  ${arr[@]};

  if [ -f $mutpath/$ctype.txt.gz ] # check if the mutation file exists
  then 

    # select mutations that overlap with 2500 promoter region as well as DNase region
    $bedtools/intersectBed -wa -a $mutpath/$ctype.txt.gz -b $dnasePath/${arr[1]} -f 1.0 | $bedtools/intersectBed -wa -a stdin -b $promoter -f 1.0 | sed 's/^chr//g' | perl -sane '$F[1]-=2;$F[2]++;$t=join("_",@F);print $F[0],"\t",$F[1],"\t",$F[2],"\t",$t,"\t",join("\t",@F[3..$#F]),"\n"' | $bedtools/fastaFromBed -bed stdin -fi $hg19Fasta -fo stdout -name -tab | sed 's/_/\t/g' >$outPath/${ctype}_DHSPROM_sig.bed

    # select mutations that overlap with 2500 promoter region but not in DNase region
    $bedtools/intersectBed -wa -a $mutpath/$ctype.txt.gz -b $dnasePath/${arr[1]} -f 1.0 -v | $bedtools/intersectBed -wa -a stdin -b $promoter -f 1.0 | sed 's/^chr//g' | perl -sane '$F[1]-=2;$F[2]++;$t=join("_",@F);print $F[0],"\t",$F[1],"\t",$F[2],"\t",$t,"\t",join("\t",@F[3..$#F]),"\n"' | $bedtools/fastaFromBed -bed stdin -fi $hg19Fasta -fo stdout -name -tab | sed 's/_/\t/g' >$outPath/${ctype}_noDHSPROM_sig.bed

    # select mutations from the entire cohort
    zcat $mutpath/$ctype.txt.gz | sed 's/^chr//g' | perl -sane '$F[1]-=2;$F[2]++;$t=join("_",@F);print $F[0],"\t",$F[1],"\t",$F[2],"\t",$t,"\t",join("\t",@F[3..$#F]),"\n"' | $bedtools/fastaFromBed -bed stdin -fi $hg19Fasta -fo stdout -name -tab | sed 's/_/\t/g' >$outPath/${ctype}_sig.bed

  fi

done

# collect the results and compute the probability
perl $path/scripts/sigCount.pl $outPath >$mutpath/signature_probabilities.tsv

# gzip the bed files
gzip -f $outPath/*.bed
