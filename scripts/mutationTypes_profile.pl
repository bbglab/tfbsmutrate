#!/usr/bin/perl 

# script to collect results of per mutation type analysis
# written by Sabarinathan Radhakrishnan

use strict;

my $path=$ARGV[0];
my $ctype=$ARGV[1];
my $target=$ARGV[2];
my $opath="$path/results/$ARGV[3]";
`mkdir $opath`,if(! -d $opath);

if($#ARGV<2){print "mutationTypes_profile.pl skcm tfbs-proximal metafiles/tfbs-perMutTypes\n\n";exit(0);}

# six different mutation types
my %hash=qw(CT GA CG GC CA GT TA AT TG AC TC AG);

my %pos;my %cnt;
# read all TFBS output to get the total number of nucleotides in each position
open(IN,"$path/results/$target/$ctype/allTFs_DHS.csv");
while(<IN>)
{
  chomp $_;
  next, if($_=~/^motif/);
  my @s=split(/\s+/, $_);
  foreach my $bp(keys(%hash))
  {
   $pos{$s[2]}{"$bp"} = 0;
   $pos{$s[2]}{"$hash{$bp}"} = 0;
   $cnt{$s[2]}=$s[3];
  }
}
close IN;

open(IN,"zcat $path/results/$target/$ctype/perSample/DHS_mutated_mutationTypes.bed.gz |"); # per mutation type count
while(<IN>)
{
 $_=~s/^\s+//g;
 my @s=split(/\s+/,$_);
 $pos{$s[1]}{"$s[3]$s[4]"} += $s[0];
}


open(OUT, ">$opath/$ctype\_mutTypes.csv") or die ("error in opening the output file");
# print header
print OUT "pos\t",join("\t",sort(keys(%hash))),"\n";
foreach my $key (sort {$a <=>$b } keys(%pos))
{
 print OUT $key,"\t";
  foreach my $bp (sort(keys(%hash)))
  {
   my $count=$pos{$key}{$bp}+$pos{$key}{$hash{$bp}};
   my $avg=$count/$cnt{$key};
   print OUT $avg,"\t";
  }
  print OUT "\n";
}
close OUT;

`gzip -f $opath/$ctype\_mutTypes.csv`
