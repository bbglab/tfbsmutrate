#!/bin/bash


# script to map the XR-seq results 
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -m <TFname> -t <targetAnalysis> -c <ctype> -a <analysisType> -x <xrseqCellType>
	-h  display this help and exit
	-m  motif name (CTCF)
        -t  target analysis (proximal)
	-a  analysis type (DHS)
        -x  (optional) xrseqCellType (CPD1h)
        -c  (optional) ctype (skcm)
Example: per_tfbsCentered_xrseq_both.sh -m ATF3 -t proximal -c skcm -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "m:t:a:c:x:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        m) tf=$OPTARG;;
        t) target=$OPTARG;;
        c) ctype=$OPTARG;;
        a) atype=$OPTARG;;
        x) xrseqCellType=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $tf || -z $atype || -z $target ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

# if no ctype given, then chose 'skcm'
if [[ -z $ctype ]];then ctype="skcm";fi 
# if no xrseqcell type given, then select all
if [[ -z $xrseqCellType ]];
then 
 xcelltypes=('CPD1h' 'CSBCPD' 'PP641h' 'CSB64')
 if [[ $target == 'transcribed' ]];
 then
  xcelltypes=('XPC64' 'XPCCPD')
 fi
else
 xcelltypes=($xrseqCellType)
fi


# make a tmp directory
if [ -d "/scratch/" ];
 then 
   tmpdir=$(mktemp -d -p /scratch/);
 else
   tmpdir=$(mktemp -d);
fi

xrseqPath="/projects_bg/bg/users/sabari/noncoding/tfbsMutationRate/pipeline/dataset/xr-seq" # path for xr-seq normalized read files

# target matches
declare -A tmatch=(["proximal"]="tfbs-proximal" ["distal"]="tfbs-distal" ["bindStrength"]="tfbs-bs-seperated" ["transcribed"]="tfbs-tss-downstream" ["dhsCentered"]="dhsCentered")
target=${tmatch[$target]} 

# output directory
if [ ! -d "$path/results/$target/$ctype/xr-seq" ];then mkdir "$path/results/$target/$ctype/xr-seq";fi

if [[ $target == 'tfbs-proximal' || $target == 'tfbs-distal' || $target == 'tfbs-tss-downstream' ]];
then
# get the previous results of TFs with observed mutations mapped 
tfbsopath="$path/results/$target/$ctype"
if [ ! -f $tfbsopath/${tf}_${atype}.bed.gz ];then echo "tfbs file not exists in the path $tfbsopath/${tf}_${atype}.bed.gz";exit;fi
zcat $tfbsopath/${tf}_${atype}.bed.gz | perl -sane '$F[1]--;print join("\t",@F),"\n";' >$tmpdir/${tf}_${atype}.bed

for xrseqCellType in ${xcelltypes[@]};
do 
  # create per xrseqcell type output directory
  if [ ! -d "$path/results/$target/$ctype/xr-seq/$xrseqCellType" ];then mkdir "$path/results/$target/$ctype/xr-seq/$xrseqCellType";fi
  opath="$path/results/$target/$ctype/xr-seq/$xrseqCellType"

  for strand in "PLUS" "MINUS";
  do

    outfile="${tf}_${atype}_${xrseqCellType}_${strand}"
    ## mapping
    $bedtools/intersectBed -wo -a $tmpdir/${tf}_${atype}.bed -b $xrseqPath/GSE67941_${xrseqCellType}_Merged_${strand}_UNIQUE_NORM_fixedStep_25.bed.gz -f 1.0 -sorted >$tmpdir/${outfile}.bed
    ## comput the repair rate
    echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv;
    cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[9];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$tmpdir/${outfile}.csv;
  done

### copy the results
mv $tmpdir/*.csv $opath/.
gzip -9 $tmpdir/*${xrseqCellType}*.bed
mv $tmpdir/*${xrseqCellType}*.bed.gz $opath/.
done

elif [[ $target == 'tfbs-bs-seperated' ]]
then

# get the previous results of TFs with observed mutations mapped 
tfbsopath="$path/results/$target/$ctype"

for qrt in 0 1 2 3;
do
 if [ ! -f $tfbsopath/${tf}_${atype}_${qrt}.bed.gz ];then echo "tfbs file not exists in the path $tfbsopath/${tf}_${atype}_${qrt}.bed.gz";exit;fi
 zcat $tfbsopath/${tf}_${atype}_${qrt}.bed.gz | perl -sane '$F[1]--;print join("\t",@F),"\n";' >$tmpdir/${tf}_${atype}_${qrt}.bed

 for xrseqCellType in ${xcelltypes[@]};
 do 
  # create per xrseqcell type output directory
  if [ ! -d "$path/results/$target/$ctype/xr-seq/$xrseqCellType" ];then mkdir "$path/results/$target/$ctype/xr-seq/$xrseqCellType";fi
  opath="$path/results/$target/$ctype/xr-seq/$xrseqCellType"

  for strand in "PLUS" "MINUS";
  do

    outfile="${tf}_${atype}_${qrt}_${xrseqCellType}_${strand}"
    ## mapping
    $bedtools/intersectBed -wo -a $tmpdir/${tf}_${atype}_${qrt}.bed -b $xrseqPath/GSE67941_${xrseqCellType}_Merged_${strand}_UNIQUE_NORM_fixedStep_25.bed.gz -f 1.0 -sorted >$tmpdir/${outfile}.bed
    ## comput the repair rate
    echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv;
    cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[9];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$tmpdir/${outfile}.csv;
  done

  ### copy the results
  mv $tmpdir/*.csv $opath/.
  gzip -9 $tmpdir/*${xrseqCellType}*.bed
  mv $tmpdir/*${xrseqCellType}*.bed.gz $opath/.
 done
done

elif [[ $target == 'dhsCentered' ]]
then

# get the previous results of DHS centered analysis with observed mutations mapped 
dhsopath="$path/results/$target/$ctype"
if [ ! -f $dhsopath/${ctype}_${atype}.bed.gz ];then echo "dhs file not exists in the path $dhsopath/${ctype}_${atype}.bed.gz";exit;fi
zcat $dhsopath/${ctype}_${atype}.bed.gz | perl -sane '$F[1]--;print join("\t",@F),"\n";' >$tmpdir/${ctype}_${atype}.bed

for xrseqCellType in ${xcelltypes[@]};
do 
  # create per xrseqcell type output directory
  if [ ! -d "$path/results/$target/$ctype/xr-seq/$xrseqCellType" ];then mkdir "$path/results/$target/$ctype/xr-seq/$xrseqCellType";fi
  opath="$path/results/$target/$ctype/xr-seq/$xrseqCellType"

  for strand in "PLUS" "MINUS";
  do

    outfile="${ctype}_${atype}_${xrseqCellType}_${strand}"
    ## mapping
    $bedtools/intersectBed -wo -a $tmpdir/${ctype}_${atype}.bed -b $xrseqPath/GSE67941_${xrseqCellType}_Merged_${strand}_UNIQUE_NORM_fixedStep_25.bed.gz -f 1.0 -sorted >$tmpdir/${outfile}.bed
    ## comput the repair rate
    echo -e "motif_name\tposition\tbp\tcnt\tAvg" >$tmpdir/${outfile}.csv;
    cat $tmpdir/${outfile}.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[9];END{foreach $d (keys(%bp)){print "$ctype\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$tmpdir/${outfile}.csv;
  done

### copy the results
mv $tmpdir/*.csv $opath/.
gzip -9 $tmpdir/*${xrseqCellType}*.bed
mv $tmpdir/*${xrseqCellType}*.bed.gz $opath/.
done

fi

rm -rf $tmpdir
