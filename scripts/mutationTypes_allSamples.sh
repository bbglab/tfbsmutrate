#!/bin/bash

# script to perform per mutation type analysis
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -c <cancertype> -t <targetAnalysis>
	-h  display this help and exit
	-c  cancer type (e.g., skcm)
        -t  target (proximal)
Example: ${0##*/} -c skcm -t proximal
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "c:t:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $ctype || -z $target ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

# target matches
declare -A tmatch=(["proximal"]="tfbs-proximal" ["distal"]="tfbs-distal" ["bindStrength"]="tfbs-bs-seperated")
target=${tmatch[$target]} 

# main paths
resPath="$path/results/$target/$ctype/perSample";

for atype in DHS;
do
# extract the mutated positions
$bedtools/intersectBed -wo -a $resPath/${atype}_mutated.bed.gz -b $mutpath/${ctype}.txt.gz -f 1.0 | cut -f 4,5,10,11 | sort | uniq -c | sort -n | gzip >$resPath/${atype}_mutated_mutationTypes.bed.gz

# generate the profile
scripts/mutationTypes_profile.pl $path $ctype $target metafiles/tfbs-perMutTypes
done
