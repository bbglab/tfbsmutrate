#!/usr/bin/perl -w

use strict;

my @nuc=qw(A C G T);

use Data::Dumper;

my %sig;my %sigRevCom;
foreach my $n (@nuc) # 5'nucl
{
  foreach my $nn (@nuc) # nucl to be mutated
  {
    foreach my $nnn (@nuc) # 3'nucl
    { 
      foreach my $mut(@nuc) # make 3 possible mutations
      { if($mut ne $nn){
         my $tri="$n$nn$nnn-$mut";
         $sig{$tri}=0;
         my $revcom="$nnn$nn$n-$mut";$revcom=~tr/ATGC/TACG/; $sigRevCom{$tri}=$revcom;
        }
      }
    }
  }
}

my @files=`ls $ARGV[0]/*.bed`;

my %hash;
foreach my $file (@files)
{
 chomp $file;
 open(IN,$file);
 my $name=(split (/\//, $file))[-1];$name=~s/\_sig.bed//g;
 #initialize
 foreach my $key (keys(%sig)){$hash{$name}->{$key}=0;}
 while(<IN>)
 {
  chomp $_;
  my @s=split(/\t/,$_);
  $hash{$name}->{"$s[7]-$s[4]"}++; 
  $hash{$name}->{"total"}++;
 }
 close IN;
}

print "Signature_reference\tSignature_alternate\tAlternate";
foreach my $name (sort(keys(%hash)))
{
 print "\tCounts_$name\tProbability_$name";
}
print "\n";
foreach my $key (sort(keys(%sig)))
{
  my $flag=0;
  my @s=split(/\-/, $key);
  my $first=substr($s[0],0,1);
  my $last=substr($s[0],-1);
  print "$s[0]\t$first$s[1]$last\t$s[1]";
  foreach my $name (sort(keys(%hash)))
  {
   print "\t",$hash{$name}{$key},"\t",(($hash{$name}{$key}+$hash{$name}{$sigRevCom{$key}})/$hash{$name}{"total"})/2;
  }
  print "\n";
}
