#!/usr/bin/perl -w

# script to collect dhs centered mutation rate results in a metafile
# written by Sabarinathan Radhakrishnan

use strict;
use Getopt::Std;

sub usage(){print "$0 -c <cancerType>\n";exit;}

my %opts;
getopt('cta',\%opts);


my $path="./"; # parent directory
my $ctype=$opts{c}; # cancer type

my $flank=1000;

# create a metafile folder
if (!-d "$path/results/metafiles"){`mkdir $path/results/metafiles`;}
if (!-d "$path/results/metafiles/dhsCentered/"){`mkdir $path/results/metafiles/dhsCentered/`;}
my $opath = "$path/results/metafiles/dhsCentered/";

# assign full name for each analysis type
my %names=qw(all all-DHS noPromoter_noTFBS DHS-noPromoters-noTFBS Promoter_noTFBS DHS-Promoters-noTFBS Promoter_TFBS DHS-Promoters-TFBS Promoter_predTFBS DHS-Promoters-predTFBS noPromoter_predTFBS DHS-noPromoters-predTFBS noPromoter_TFBS DHS-noPromoters-TFBS noPromoter_predTFBSAll DHS-noPromoters-predTFBSAll Promoter_predTFBSAll DHS-Promoters-predTFBSAll);

# read all the output files
my %hash;
foreach my $type ("all", "noPromoter_noTFBS", "Promoter_noTFBS", "Promoter_TFBS", "Promoter_predTFBS", "Promoter_predTFBSAll", "noPromoter_predTFBS", "noPromoter_TFBS", "noPromoter_predTFBSAll")
{
  my $filename="$path/results/dhsCentered/$ctype/$ctype\_DHS_$type.csv";
  open(IN, $filename) or die ("error in opening the input file $filename");
  while(<IN>)
  {
   next, if ($_=~/^ctype/);
   chomp $_;
   my @s=split(/\s+/, $_);
   $hash{$type}->{$s[1]}=$s[4]; 
  }
}

# print the header
open(OUT,">$opath/$ctype\_DHS.csv");
my $val="position";
foreach my $type ("all", "noPromoter_noTFBS", "Promoter_noTFBS", "Promoter_TFBS", "Promoter_predTFBS", "Promoter_predTFBSAll", "noPromoter_predTFBS", "noPromoter_TFBS", "noPromoter_predTFBSAll")
{
  $val.="\t".$ctype."_".$names{$type};
}
print OUT $val,"\n";

# print each row as position, and columns as different analysis
for(my $i=-$flank;$i<=$flank;$i++)
{
my $val=$i;
foreach my $type ("all", "noPromoter_noTFBS", "Promoter_noTFBS", "Promoter_TFBS", "Promoter_predTFBS", "Promoter_predTFBSAll", "noPromoter_predTFBS", "noPromoter_TFBS", "noPromoter_predTFBSAll")
{
  $val.="\t".$hash{$type}->{$i};
}
print OUT $val,"\n";
}
close OUT;

`gzip -f $opath/$ctype\_DHS.csv`;
