#!/usr/bin/perl -w

# script to collect the results of XR-seq mapping in a metafile
# written by Sabarinathan Radhakrishnan

use warnings 'all';
use strict;
use FindBin;

# read config file to get the main path 
open(IN,"$FindBin::Bin/config.file") or die ("error in opening the config file");
my %VAR; # get variables from config file
while(<IN>){chomp;$_=~s/\s+//g;if($_=~/=/){my @s=split(/\=/, $_);$VAR{$s[0]}=$s[1];}}
close IN;
die "error in retreiving the main path $VAR{path}", if(! -d $VAR{path});

use Getopt::Std;
sub usage(){print "$0 -c <cancerType> -t <targetAnalysis> -a <analysisType> -o <outputDir>\n";exit;}

my %opts;
getopt('sctaoe',\%opts);

&usage, if(scalar(keys(%opts))>5 || scalar(keys(%opts))<4);

my $ctype=$opts{c}; # cancer type
my $atype=$opts{a}; # DHS or noDHS
my $target=$opts{t}; # target analysis (proximal|distal)
my $moutput=$opts{o}; # output metafile directory

# target matches
my %tmatch=qw(proximal tfbs-proximal distal tfbs-distal bindStrength tfbs-bs-seperated transcribed tfbs-tss-downstream);
$target=$tmatch{$target};

my @cellTypes;
my %names;
if ($target!~/tfbs-tss-downstream/)
{
 @cellTypes=qw(CPD1h CSB64 CSBCPD PP641h);
 %names=qw(CPD1h NHF1_CPD CSBCPD CSB_CPD CSB64 CSB_PP64 PP641h NHF1_PP64);
}
else
{
 @cellTypes=qw(XPCCPD XPC64);
 %names=qw(XPCCPD XPC_CPD XPC64 XPC_PP64);
}
my @strands=qw(PLUS MINUS);

# results directory
my $path="$VAR{path}/results/$target/$ctype/xr-seq/";

# output directory
my $opath="$VAR{path}/results/$moutput/xr-seq";
`mkdir $opath`, if(! -d $opath);

my $flank=$VAR{flank};
print $flank;

foreach my $cell (@cellTypes)
{ 
  my %hash=();
  my %tflist;
  print $cell,"\n";
  # read the average repair rate for each TF and two strand, and save it in a hash
  foreach my $tfFile (`ls $path/$cell/*_$atype\_*.csv`)
  { 
    chomp $tfFile;
    my $tf;my $st;
    # get tf and strand information from filename 
    if ($target !~ /tfbs-bs-seperated/)
    {
       ($tf,$st) = (split(/\_/, (split(/\//, $tfFile))[-1]))[0,3]; # get tf and strand information from filename
    }
    else
    { my $qrt;
     ($tf,$qrt,$st) = (split(/\_/, (split(/\//, $tfFile))[-1]))[0,2,4]; # get tf and strand information from filename
     $tf=$tf."_".$qrt;
    }
    $st=~s/\.csv//g;
    # initialize
    for(my $i=-$flank;$i<=$flank;$i++)
    { $hash{$i}->{$tf}->{$st}=0; }

    # per TF output
    open(IN, $tfFile ) or die "error in opening the input file $tfFile";
    while(<IN>)
    { 
      next, if ($_=~/motif/);
      my @s=split(/\s+/, $_);
      $hash{$s[1]}->{$tf}->{$st}=$s[4];
    }
    close IN;
    $tflist{$tf}=1;
  }

  # print a metafile with repair rate for every TF and allTFs together in a column-wise file
  foreach my $str (@strands)
  {
   open(OUT,">$opath/TFBS_$atype\_$names{$cell}\_$str.csv");
   print OUT "position\t",join("\t",sort(keys(%tflist))),"\n";
   for(my $i=-$flank;$i<=$flank;$i++)
   { 
    my $val="$i";
    # per TF mapping
    foreach my $tf (sort(keys(%tflist)))
    { 
      $val.= "\t". $hash{$i}->{$tf}->{$str};
    }
    print OUT $val,"\n";
   }
   close OUT;
   `gzip -f $opath/TFBS_$atype\_$names{$cell}\_$str.csv`;
  }
  
  # print a file for combined strand value 
  open(OUT,">$opath/TFBS_$atype\_$names{$cell}_combined.csv");
  print OUT "position\t",join("\t",sort(keys(%tflist))),"\n";
  # combine two strands 
  for(my $i=-$flank;$i<=$flank;$i++)
  { 
    my $val="$i";
    # per TF mapping
    foreach my $tf (sort(keys(%tflist)))
    { 
      my $avg = $hash{$i}->{$tf}->{"PLUS"}+$hash{$i}->{$tf}->{"MINUS"};
      $val.="\t".($avg/2);
    }
    print OUT $val,"\n";
  }
  close OUT;

  `gzip -f $opath/TFBS_$atype\_$names{$cell}_combined.csv`;
}
