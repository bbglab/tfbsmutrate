#!/bin/bash

# script to prepare data for per mutation type and per sample analysis
# written by Sabarinathan Radhakrishnan

# read configuration file
sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -c <cancertype> -t <targetAnalysis> -a <analysisType>
	-h  display this help and exit
	-c  cancer type (e.g., skcm)
	-t  target analysis (proximal|bound-unbound|bindStrength|distal|transcribed)
	-a  TFBS overlap with DHS or not (DHS|noDHS)
Example: ${0##*/} -c skcm -t proximal -a DHS
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

while getopts "c:t:a:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        t) target=$OPTARG;;
        a) atype=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $ctype || -z $target || -z $atype ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

mutPath="$path/datasets/mutations/"; # path for the mutation data file
resmotif="$path/results/tfbs-${target}"; # path to access the results of TFs
resPath="$path/results/tfbs-${target}/$ctype/perSample"; # path to write the output


if [ ! -d $resPath ];then mkdir $resPath;fi

# make a tmp directory
if [ -d "/scratch/sabari/" ]; then
   tmpdir=$(mktemp -d -p /scratch/sabari);
 else
   tmpdir=$(mktemp -d);
fi

# extract the mutated positions
zcat $resmotif/$ctype/*_${atype}.bed.gz | awk '$6>0' | sort -u -T $tmpdir >$tmpdir/${atype}_mutated.bed
gzip -f $tmpdir/${atype}_mutated.bed

#echo "get the nonmutated position counts..."
zcat $resmotif/$ctype/*_${atype}.bed.gz | awk '$6==0' | sort -u -T $tmpdir | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[5];END{foreach $d (keys(%bp)){print $ctype,"\t",$d,"\t",$cnt{$d},"\t",$bp{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype >$tmpdir/${atype}_nonmutated.cnt;

mv $tmpdir/${atype}_mutated.bed.gz $tmpdir/${atype}_nonmutated.cnt $resPath/
