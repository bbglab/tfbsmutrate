#!/bin/bash

# script to compute mutation rate at DHS centered region
# written by Sabarinathan Radhakrishnan

sourcedir=$(dirname $BASH_SOURCE) # find the path where scripts are available
source $sourcedir/config.file # read config file to get data path variables

show_help() {
cat << EOF
Usage: ${0##*/} -c <cancertype> -a <analysisType>
	-h  display this help and exit
	-c  cancer type (e.g., skcm)
        -a  analysis type
	-n  number of cpus (default: 4)
Example: dhsCenteredAnalysis.sh -c skcm -a DHS_all
EOF
}

if [[ $# == 0 ]];then show_help;exit 1;fi

background=0
while getopts "c:a:n:h" opt; do
    case "$opt" in
        h) show_help;exit 0;;
        c) ctype=$OPTARG;;
        a) atype=$OPTARG;;
        n) ncpus=$OPTARG;;
       '?')show_help >&2 exit 1 ;;
    esac
done

if [[ -z $ctype || -z $atype ]];then 
   echo -e "\n\n\t***input missing check the usage below**\n\n"; show_help; exit 1;
fi

if [[ -z $ncpus ]];then ncpus=4;fi

# make a tmp directory
if [ -d "/scratch/" ]; then 
   tmpdir=$(mktemp -d -p /scratch/)
 else
   tmpdir=$(mktemp -d)
fi

# get DHS file for the given ctype
if [[ $ctype =~ "crcH" ]];
then 
  dnaseFile=`grep -w ${ctype:0:3} $dnasePath/DHS_list.txt | cut -f3`;
else
  dnaseFile=`grep -w $ctype $dnasePath/DHS_list.txt | cut -f3`;
fi

if [ ! -f $dnasePath/$dnaseFile ];then echo "DHS file don't exists for $ctype";exit;fi

TFBSfile="$tfbsPath/merged_ENCODE.tf.bound.union.bed.gz" # merged TFBS sites from ENCODE
predTFBSfile="$tfbsPath/predictedMotifs_PIQ.bed.gz" #predicted TFBS sites by PIQ

# create output directory
if [ ! -d "$path/results/dhsCentered" ];then mkdir "$path/results/dhsCentered";fi
if [ ! -d "$path/results/dhsCentered/$ctype" ];then mkdir "$path/results/dhsCentered/$ctype";fi

# Extend DHS from its mid-point 
# ==============================
# take +/- 1000 (flank) nucleotides from DHS mid-point
# all DHS genome wide
if [[ $atype == "DHS_all" ]];then 
 zcat $dnasePath/$dnaseFile  | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS outside promoters (distal) and don't have sequences of known TFBS. The later part is performed in the below analysis function.
elif [[ $atype == "DHS_noPromoter_noTFBS" ]];then 
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -v -sorted | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS outside promoters (distal) and overlap the predicted TFBS. 
# The DHS regions which contains sequences of any known TFBS are filtered out in the below analysis function.
elif [[ $atype == "DHS_noPromoter_predTFBS" || $atype == "DHS_noPromoter_predTFBSAll" ]];then
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -v -sorted | $bedtools/intersectBed -wa -a stdin -b $predTFBSfile -u -sorted | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS outside promoter (distal) and overlap known TFBS. 
elif [[ $atype == "DHS_noPromoter_TFBS" ]];then
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -v -sorted | $bedtools/intersectBed -wa -a stdin -b $TFBSfile -u -sorted | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS overlap promoters, but removed for sequences of any known or predicted TFBS. The later part is performed in the below analysis function.
elif [[ $atype == "DHS_Promoter_noTFBS" ]];then
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -u -sorted | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS overlap promoter and predicted TFBS. 
# The DHS regions which contains sequences of any known TFBS are filtered out in the below analysis function.
elif [[ $atype == "DHS_Promoter_predTFBS" || $atype == "DHS_Promoter_predTFBSAll" ]];then
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -u -sorted | $bedtools/intersectBed -wa -a stdin -b $predTFBSfile -u -sorted | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

# DHS overlap promoter and known TFBS
elif [[ $atype == "DHS_Promoter_TFBS" ]];then
 # first get the TFBS that are inside the promoter region
 $bedtools/intersectBed -wa -a $TFBSfile -b $promoter -f 0.5 -u >$tmpdir/tmp_tfbs.bed
 $bedtools/intersectBed -wa -a $dnasePath/$dnaseFile -b $promoter -u -sorted | $bedtools/intersectBed -wa -a stdin -b $tmpdir/tmp_tfbs.bed -u | $bedtools/sortBed | perl -sane '$midpoint=int(($F[2]+$F[1])/2);print $F[0],"\t",$midpoint,"\t",$midpoint,"\t",join("\t",@F[3..$#F]),"\n";' | $bedtools/slopBed -i stdin -g $hg19genome -b $flank >$tmpdir/tmp_flank.bed

else
   echo "no matching";exit;
fi


# get the number of chromosome details
allchr=(`cut -f1 $tmpdir/tmp_flank.bed | sort -u | sort -k1,1`)

# function to perform the analysis per chromosome wise
analysis()
{
# Get the tri-nucleotide info.
# ============================
# first extract the sequence for the selected chromosome
chr=$1
#mem=$perprocmem # for sort function to speedup; numbers in gb
awk 'BEGIN{OFS="\t";} {$2-=2;$3+=1;print $1,$2,$3;}' $tmpdir/tmp_flank.bed | grep -w $chr | sed 's/chr//g' | $bedtools/fastaFromBed -fi $hg19Fasta -bed stdin -fo $tmpdir/${chr}_seq.txt

# convert trinucleotides into numeric ids
perl $path/scripts/makeTriNucProfile_motifs_v3.pl DHS $tmpdir/${chr}_seq.txt $flank | sort -k2,2n >$tmpdir/${chr}_tmp.bed

# Apply filters
# =============
# filter if any position that overlap with cds, blacklisted region and low mappbility
$bedtools/intersectBed -wa -a $tmpdir/${chr}_tmp.bed -b $blackListV1 -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $cds -f 1.0 -v -sorted | $bedtools/intersectBed -wa -a stdin -b $mappable36mer -f 1.0 -sorted >$tmpdir/${chr}_tmp_filtered.bed

if [[ $atype == "DHS_Promoter_noTFBS" ]];then
$bedtools/intersectBed -wa -a $tmpdir/${chr}_tmp_filtered.bed -b $TFBSfile -v -sorted | $bedtools/intersectBed -wa -a stdin -b $predTFBSfile -v -sorted >$tmpdir/${chr}_tmp_filtered_tfbs.bed
mv $tmpdir/${chr}_tmp_filtered_tfbs.bed $tmpdir/${chr}_tmp_filtered.bed
elif [[ $atype == "DHS_Promoter_predTFBS" || $atype == "DHS_noPromoter_predTFBS" ]];then
$bedtools/intersectBed -wa -a $tmpdir/${chr}_tmp_filtered.bed -b $TFBSfile -v -sorted >$tmpdir/${chr}_tmp_filtered_tfbs.bed
mv $tmpdir/${chr}_tmp_filtered_tfbs.bed $tmpdir/${chr}_tmp_filtered.bed
elif [[ $atype == "DHS_noPromoter_noTFBS" ]];then
$bedtools/intersectBed -wa -a $tmpdir/${chr}_tmp_filtered.bed -b $promoter -v -sorted | $bedtools/intersectBed -wa -a stdin -b $TFBSfile -v -sorted | $bedtools/intersectBed -wa -a stdin -b $predTFBSfile -v -sorted >$tmpdir/${chr}_tmp_filtered_notfbs.bed
mv $tmpdir/${chr}_tmp_filtered_notfbs.bed $tmpdir/${chr}_tmp_filtered.bed
fi

# additionaly, filter if the flank regions overlap with other DHS regions
# first seperate the core motif positions
awk '$4>=-75 && $4<=75' $tmpdir/${chr}_tmp_filtered.bed >$tmpdir/${chr}_tmp_core.bed
# filter the flanks if they overlap with other DHS regions
awk '$4<-75 || $4>75' $tmpdir/${chr}_tmp_filtered.bed | $bedtools/intersectBed -wa -a stdin -b $dnasePath/$dnaseFile -f 1.0 -v -sorted >>$tmpdir/${chr}_tmp_core.bed

# map and count the number of mutations that overlap each position
$bedtools/intersectBed -c -a $tmpdir/${chr}_tmp_core.bed -b $mutpath/$mutationdata/$ctype.txt.gz -f 1.0 | sort -k2,2n >$tmpdir/${chr}_analysis.bed
cat $tmpdir/${chr}_analysis.bed | perl -sane '$bp{$F[3]}++;$cnt{$F[3]}+=$F[5];END{foreach $d (keys(%bp)){print $ctype,"\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2 | grep -v pos >$tmpdir/${chr}_analysis.csv

rm -rf ${chr}_tmp_filtered.bed ${chr}_tmp_core.bed $tmpdir/${chr}_tmp.bed
}

# perform the analysis chromosome wise in parallel
func_parallel analysis "$(echo ${allchr[@]})" $ncpus

## Compute Mutation rate for whole genome
echo -e "ctype\tposition\tbp\tcnt\tAvg" >$tmpdir/${ctype}_${atype}.csv
cat $tmpdir/chr*analysis.csv | grep -v pos | perl -sane '$bp{$F[1]}+=$F[2];$cnt{$F[1]}+=$F[3];END{foreach $d (keys(%bp)){print $ctype,"\t",$d,"\t",$bp{$d},"\t",$cnt{$d},"\t",($cnt{$d}/$bp{$d}),"\n";}}' -- -ctype=$ctype | sort -n -k2,2  >>$tmpdir/${ctype}_${atype}.csv
mv $tmpdir/${ctype}_${atype}.csv $path/results/dhsCentered/$ctype/.

## compress the big files
outfile="${ctype}_${atype}.bed"
for chr in ${allchr[@]};
do
cat $tmpdir/${chr}_analysis.bed >>$tmpdir/$outfile
rm -rf $tmpdir/${chr}_analysis.bed;
done
gzip $tmpdir/$outfile
mv $tmpdir/${outfile}.gz $path/results/dhsCentered/$ctype/.

# remove the tmp directory
rm -rf $tmpdir
